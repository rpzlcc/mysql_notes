# Chapter 2: Writing MySQL-Based Programs

## 2.1. Connecting, Selecting a Database, and Disconnecting

```python
import mysql.connector

try:
    conn = mysql.connector.connect(database = "cookbook",
                                   host="localhost",
                                   user="cbuser",
                                   password="cbpass")
    print("connected")
except:
    print("cannot connect to server")
else:
    conn.close()
    print("disconnected")

# specify parameters using Pythin dictionary
conn_params = {
    "database": "cookbook",
    "host": "localhost",
    "user": "cbuser",
    "password": "cbpass",
}
conn = mysql.connector.connect(**conn_params)
print("connected")
```

## 2.2. Checking for Errors

```python
conn_params = {
    "database": "cookbook",
    "host": "localhost",
    "user": "baduser",
    "password": "badpass"
}

try:
    conn = mysql.connector.connect(**conn_params)
    print("Connected")
except mysql.connector.Error as e:
    print("Cannot connect to server")
    print("Error code: %s" % e.errno)
    print("Error message: %s" % e.msg)
    print("Error SQLSTATE: %s" % e.sqlstate)
```

## 2.3. Writing Library Files

```python
# cookbook.py: library file with utility method for connecting to MySQL
# using the Connector/Python module
import mysql.connector

conn_params = {
    "database": "cookbook",
    "host": "localhost",
    "user": "cbuser",
    "password": "cbpass",
}
# Establish a connection to the cookbook database, returning a connection
# object. Raise an exception if the connection cannot be established.


def connect():
    return mysql.connector.connect(**conn_params)

# in another file
import mysql.connector
import cookbook

try:
    conn = cookbook.connect()
    print("Connected")
except mysql.connector.Error as e:
    print("Cannot connect to server")
    print("Error code: %s" % e.errno)
    print("Error message: %s" % e.msg)
else:
    conn.close()
    print("Disconnected")

```

## 2.4. Executing Statements and Retrieving Results

```python
cursor1 = conn.cursor()
cursor1.execute("update profile set cats = cats + 1 where name = 'Sybil'")
print("number of rows updated: {}".format(cursor1.rowcount))
cursor1.close()
conn.commit()

# If the statement returns a result set, fetch its rows, then close the cursor.
# The fetchone() method returns the next row as a sequence, or None when there are no more rows:
cursor2 = conn.cursor()
cursor2.execute("select id, name, cats from profile")
while True:
    row = cursor2.fetchone()
    if row is None:
        break
    print("id: {}, name: {}, cats: {}".format(row[0], row[1], row[2]))
print("number of rows returned: {}".format(cursor2.rowcount))
cursor2.close()

# use the cursor itself as an iterator that returns each row in turn:
cursor3 = conn.cursor()
cursor3.execute("select id, name, cats from profile")
for (id, name, cats) in cursor3:
    print("id: {}, name: {}, cats: {}".format(id, name, cats))
print("number of rows returned: {}".format(cursor3.rowcount))
cursor3.close()

# The fetchall() method returns the entire result set as a sequence of row sequences.
# Iterate through the sequence to access the rows:
cursor4 = conn.cursor()
cursor4.execute("select id, name, cats from profile")
rows = cursor4.fetchall()
for row in rows:
    print("id: {}, name: {}, cats: {}".format(row[0], row[1], row[2]))
print("number of rows returned: {}".format(cursor4.rowcount))
cursor4.close()
```

## 2.5. Handling Special Characters and NULL Values in Statements

```python
cursor = conn.cursor()
cursor.execute('''
               insert into profile (name, birth, color, foods, cats)
               values ({}, {}, {}, {}, {})
               '''.format("De'Mont", "1973-01-12", None, "eggroll", 4))
cursor.close()
conn.commit()
```
```sql
-- final SQL sent to server:
INSERT INTO profile (name,birth,color,foods,cats) VALUES('De\'Mont','1973-01-12',NULL,'eggroll',4)
```

## 2.6. Handling Special Characters in Identifiers

```sql
create table `some table` (i int);

-- be aware of ANSI_QUOTES
```

## 2.7. Identifying NULL Values in Result Sets

In Python, None means NULL.

```python
cursor = conn.cursor()
cursor.execute("select name, birth, foods from profile")
for row in cursor:
    row = list(row)
    for i, value in enumerate(row):
        if value is None:
            row[i] = "NULL"
    print("name: {}, birth: {}, foods: {}".format(row[0], row[1], row[2]))
cursor.close()
```

## 2.8. Techniques for Obtaining Connection Parameters

Problem:

* You need to obtain connection parameters for a script so that it can connect to a MySQL server.

Alternative Solutions:

* Hardwire the parameters into the program
* Ask for the parameters interactively
* Get the parameters from the command line
* Get the parameters from the execution environment
* Get the parameters from a separate file

Python does not support reading from option files.