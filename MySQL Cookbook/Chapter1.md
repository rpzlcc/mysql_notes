# Chapter 1: Using the mysql Client Program

## 1.1. Setting Up a MySQL User Account

```sql
create user 'cbuser'@'localhost' identified by 'cbpass';
grant all on cookbook.* to 'cbuser'@'localhost';
```

## 1.2. Creating a Database and a Sample Table

```sql
create database cookbook;
use cookbook;
create table limbs (thing varchar(20), legs int, arms int);
insert into limbs (things, legs, arms) values ('human', 2, 2);
INSERT INTO limbs (thing,legs,arms) VALUES('insect',6,0);
INSERT INTO limbs (thing,legs,arms) VALUES('squid',0,10);
```

## 1.3. What to Do if mysql Cannot Be Found

Set correct path

## 1.4. Specifying mysql Command Options

```sql
mysql -h localhost -u cbuser -p
mysql --host=localhost --user=cbuser --password
mysqldump -h localhost -u cbuser -p cookbook > cookbook.sql
mysqladmin -h localhost -u root -p shutdown
```

## 1.5. Executing SQL Statements Interactively

```sql
select now();
select now()\g
show full columns from limbs like 'thing'\G
mysql -e "select count(*) from limbs" cookbook
mysql -e "select count(*) from limbs; select now();" cookbook
```

## 1.6. Executing SQL Statements Read from a File or Program

```sql
mysql cookbook < limbs.sql
mysql> source limbs.sql;
mysql> \. limbs.sql;

mysqldump cookbook > dump.sql
mysql -h another-host cookbook < dump.sql

mysqldump cookbook | mysql -h another-host cookbook
```

## 1.7. Controlling mysql Output Destination and Format

```bash
mysql cookbook > outputfile
mysql cookbook < inputfile > outputfile
mysql cookbook < inputfile | mail pzhu@pzhu.com

# more readable tabular format
echo "select * from limbs where legs = 0" | mysql cookbook
mysql -t cookbook < inputfile | mail pzhu@pzhu.com # --table
mysql -B -- --batch

# HTML
mysql -H -e "select * from limbs" cookbook

# XML
mysql -X -e "select * from limbs" cookbook

# suppressing column heading
mysql --skip-column-names -e "select arms from limbs" cookbook # -N
mysql -ss -e "select arms from limbs" cookbook # --silent

# specifying output column identifier
mysql cookbook < inputfile | sed -e "s/TAB/:/g" > outputfile
mysql cookbook < inputfile | tr "TAB" ":" > outputfile
mysql cookbook < inputfile | tr "\011" ":" > outputfile

# process CSV
# 1. escape any quotes by doubling them
# 2. change tab to comma
# 3. surround column values with quotes
mysql cookbook < inputfile | sed -e 's/"/""/g' -e 's/TAB/","/g' -e 's/^/"/' -e 's/$/"/' > outputfile

# control verbose level
echo "select now()" | mysql
echo "select now()" | mysql -v
echo "select now()" | mysql -vv
echo "select now()" | mysql -vvv
```

## 1.8. Using User-Defined Variables in SQL Statements

```sql
select @max_limbs := MAX(arms + legs) from limbs;
select * from limbs where arms + legs = @max_limbs;

select @last_id := LAST_INSERT_ID();

-- user variables hold single values

-- set value explicitly
set @sum = 4 + 7;
set @max_limbs = (select max(arms + legs) from limbs);

-- user variables cannot be used in things like table names
```