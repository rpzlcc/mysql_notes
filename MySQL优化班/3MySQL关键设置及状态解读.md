# 优化班第十期第四次课

## Some Variables

* pid_file = HOSTNAME.pid --> process ID
* skip_name_resolve: disable DNS hostname lookups
* max_connect_errors: if reached this number, MySQL will block IP/hostname until "flush hosts"
* table_open_cache: ibd files; "opening table" and "closing table" if it's too small
* table_definition_cache: frm files
* table_open_cache_instances
* sort_buffer_size: no index; merge
* join_buffer_size
* thread_cache_size
* read_rnd_buffer_size
* interactive_timeout
* wait_timeout
* tmp_table_size, max_heap_table_size: < 200MB
* long_query_time
* sync_binlog
* innodb_flush_log_at_trx_commit
* expire_logs_days
* master_info_repository = TABLE
* relay_log_info_repository = TABLE
* gtid_mode = ON
* binlog_format = ROW
* relay_log_recovery = 1
* relay_log_purge = 1
* lock_wait_timeout = 3600 (default is one year)
* innodb_thread_concurrency = 0
* innodb_lock_wait_timeout = 10 (default 50)
* innodb_lru_scan_depth = 4000
* innodb_stats_persistent_sample_pages

## 个别关键status

* Aborted_connects：
* Aborted_clients: 由于客户端没有正确关闭连接导致客户端终止而中断的连接数
* Created_tmp_disk_tables: 服务器执行语句时在硬盘上自动创建的临时表的数量。是指在排序时，内存不够 用(tmp_table_size 小于需要排序的结果集)，所以需要创建基于磁盘的临时表进 行排序。
* Created_tmp_tables： 服务器执行语句时自动创建的内存中的临时表的数量。如果 Created_tmp_disk_tables 较大，你可能要增加 tmp_table_size 值使临时表基于内 存而不基于硬盘。
    * Created_tmp_disk_tables/(Created_tmp_disk_tables+Created_tmp_tables)* 100% &get; 10%的话，就需要注意适当 高 tmp_table_size 的大小 但是不能设置太大，因为它是每个 session 都会分配的，可能会导致 OOM
* Handler_read_rnd：根据固定位置读一行的请求数。如果你正执行大量查询并需要对结果进行排序该 值较高，说明可能使用了大量需要 MySQL 扫 整个表的查询或没有正确使用索 引。
    * 例如:select * from t limit 1000,1;
    * 或者:select * from t order by non_key_col limit 100,1;
* Handler_read_rnd_next: 在数据文件中读下一行的请求数。如果你正进行大量的表扫 ，该值会较高。通 常说明你的表索引不正确或写入的查询没有利用索引。
    * select * from t order by non_key_col limit 100;
* Innodb_buffer_pool_wait_free: 一般情况，通过后台向 Innodb buffer pool 写。但是，如果需要读或创建页，并且 没有干净的页可用，则它还需要先等待页面清空。该计数器对等待实例进行记数。 如果已经适当设置 Innodb buffer pool 大小，该值应小。
* Innodb_log_waits: 我们必须等待的时间，因为日志缓冲区太小，我们在继续前必须先等待对它清空。
* Innodb_row_lock_current_waits: 当前等待的待锁定的行数。
* Threads_created: 太大说明thread cache 不够用
* Open_tables, Opened_tables: Opened_tables不应该特别大
    * open-tables，表示当前正在打开的table数量
    * opened-tables，表示历史上总共打开过的table数量
* Select_full_join: 没有使用索引的联接的数量。如果该值不为 0,你应仔细检查表的索引。
* Select_scan
* Sort_merge_passes
* Table_locks_waited
* Threads_cached, Threads_connected, Threads_created
    * tread-cache-size 至少要是 thread-connected 的两倍

## 个别关键state (from show processlist)

* copy to tmp table
* copying to tmp table, copying to tmp table on disk
* copying to group table
* creating sort index, sorting result
* creating tmp table
* closing tables, opening tables
* sending data：没有使用索引；或者结果集太大
* waiting for ... lock

## 学习方法

* 手册中重要章节通读一遍
* 数据库基础理论知识：事务，锁，B+树
* 边学习理论，边动手实践
* 有条件的话，边学习源码边动手实践验证
* 遇到问题时，先观察日志，自主思考可能的原因
* 多看手册及官网等靠谱知识来源

```bash
pager cat - | grep -v Sleep | sort -rn -k 12 | head -n 20
```