# MySQL架构与历史

## 1.1 逻辑架构

![MySQL逻辑架构图](https://3.bp.blogspot.com/-WllJC9xfxqg/VxTbuAoMm4I/AAAAAAAAHzA/1tF7Sxx8Y34levMmR9fYPZfDQDHVdWzKwCLcB/s1600/MySQL%2BArchitecture.png)

第二层包含大多数核心服务，包括查询解析、分析、优化、缓存及所有的内置函数；并实现所有跨存储引擎的功能：存储过程，触发器，视图等。

第三层为存储引擎，负责数据的存储和提取。

### 1.1.1 连接管理与安全性

每个客户端连接都会在服务器进程中拥有一个线程，这个连接的查询只会在这个单独的线程中执行。

服务器对客户端连接的认证基于用户名，原始主机信息，和密码。

### 1.1.2 优化与执行

MySQL会解析查询，并创建内部数据结构（解析树），然后对其进行各种优化，包括重写查询、决定表的读取顺序，以及选择合适的索引等。

优化器不关心表使用的是什么引擎，但是引擎对于优化查询是有影响的。优化器会请求引擎提供容量或者某个具体操作的开销信息，以及表数据的统计信息等。

## 1.2 并发控制

两个层面的并发控制：服务器层和存储引擎层。

### 1.2.1 读写锁

两种类型的锁

* 共享锁，读锁：相互不阻塞
* 排它锁，写锁：一个写锁会阻塞其他的写锁和读锁。

### 1.2.2 锁粒度

理想方式是只对会修改的数据片进行精确锁定。

在给定的资源上，锁定的数量越少，系统并发程度越高，只要相互之间不发生冲突即可。

加锁也需要消耗资源。各种操作：获得锁，检查所是否已经解除，释放锁等。

锁策略：在锁的开销和数据安全性之间寻求平衡。一般都用行级锁。

#### 行级锁

最大程度支持并发处理，同时也带来最大锁开销。

## 1.3 事务

事务就是一组原子性的SQL查询，或者说一个独立的工作单元。事务中的语句，要么全部执行成功，要么全部失败。

ACID

* 原子性 Atomicity：一个事务被视为一个不可分割的最小工作单元，整个事务中的所有操作要么全部提交成功，要么全部失败回滚。对于一个事务来说，不可能只执行其中的一部分操作。
* 一致性 Consistency：数据库总是从一个一致性状态转换到另外一个一致性状态。
* 隔离性 Isolation：一个事务所做的修改在最终提交以前，对其他事务是不可见的。
* 持久性 Durability：一旦事务提交，其所做的修改就会永久保存的数据库中。此时即使系统崩溃，修改的数据也不会丢失。

### 1.3.1 隔离级别

![Isolation Levels](http://www.programering.com/images/remote/ZnJvbT1jbmJsb2dzJnVybD13WndwbUx5RURaeWNqTnhRV055WVdZdElqTmtGV0x5WUdaejB5TnhVRE50VVdNamxqWTFZV1l2QVRNMkl6TnZNV2F3OVNaeVZIZGpsR2N2UVdZdnhHYzE5U2J2Tm1MbGxYWjBsbUxzUjJMdm9EYzBSSGE.jpg)

SQL标准中定义了四种隔离级别，每一种级别都规定了一个事务中所做的修改，哪些在事务内和事务间是可见的，哪些是不可见的。较低级别的隔离通常可以执行更高的并发，系统的开销也更低。

#### READ UNCOMMITED 未提交读

事务中的修改，即使没有提交，对其他事务也都是可见的。

事务可以读取未提交的数据，被称为脏读（dirty read）。

#### READ COMMITTED 提交读

满足隔离性的简单定义：一个事务开始时，只能看见已经提交的事务所做的修改。换句话说，一个事务从开始直到提交之前，所做的任何修改对其他事务都是不可见的。

这个级别有时也称为不可重复读（nonrepeatable read），因为两次执行同样的查询，可能会得到不一样的结果。

#### REPEATABLE READ 可重复读

解决了脏读的问题。保证了在同一个事务中多次读取同样记录的结果是一致的。

理论上，可重复读还是无法解决另外一个幻读（phantom read）的问题。

幻读是指当某个事务在读取某个范围内的记录时，另外一个事务又在该范围插入了新的记录。当之前的事务再次读取该范围的记录时，会产生幻行（phantom row）。InnoDB通过MVCC解决了幻读的问题。

#### SERIALIZABLE 可串行化

最高的隔离级别。通过强制事务串行执行，避免了幻读问题。

简单来说，此级别会在读取的每一行数据上都加锁，所以可能导致大量的超时和锁争用问题。

### 1.3.2 死锁

死锁是指两个或者多个事务在同一资源上相互占用，并请求锁定对方所占有的资源，从而导致恶性循环的现象。

死锁检测和死锁超时机制：

* 检测到死锁的循环依赖以后，立即返回一个错误
* 或者：当查询的时间达到锁等待超时的设定后放弃锁请求（InnoDB选择将持有最少行级排它锁的事务回滚）

### 1.3.3 事务日志

事务日志可以帮助提高事务的效率。使用事务日志，存储引擎在修改表的数据时只需要修改其内存拷贝，再把该修改行为记录到持久在硬盘上的事务日志中，而不用每次都将修改的数据本身持久到磁盘。事务日志采用的是追加的方式，因此写日志的操作是磁盘上一小块区域内的顺序I/O，而不像随机I/O需要在磁盘的多个地方移动磁头，所以采用事务日志的方式相对来说要快得多。事务日志持久以后，内存中被修改的数据在后台可以慢慢地刷回到磁盘。目前大多数存储引擎都是这样实现的，我们通常称之为预写式日志（Write-AheadLogging），修改数据需要写两次磁盘。

如果数据的修改已经记录到事务日志并持久化，但数据本身还没有写回磁盘，此时系统崩溃，存储引擎在重启时能够自动恢复这部分修改的数据。具体的恢复方式则视存储引擎而定。

### 1.3.4 MySQL中的事务

#### 自动提交 autocommit

MySQL默认采用自动提交（AUTOCOMMIT）模式。也就是说，如果不是显式地开始一个事务，则每个查询都被当作一个事务执行提交操作。在当前连接中，可以通过设置AUTOCOMMIT变量来启用或者禁用自动提交模式。

```sql
SHOW VARIABLES LIKE 'AUTOCOMMIT';
```

当AUTOCOMMIT=0时，所有的查询都是在一个事务中，直到显式地执行COMMIT提交或者ROLLBACK回滚，该事务结束，同时又开始了另一个新事务。修改AUTOCOMMIT对非事务型的表，比如MyISAM或者内存表，不会有任何影响。

另外还有一些命令，在执行之前会强制执行COMMIT提交当前的活动事务。典型的例子，在数据定义语言（DDL）中，如果是会导致大量数据改变的操作，比如ALTER TABLE，就是如此。另外还有LOCK TABLES等其他语句也会导致同样的结果。

MySQL可以通过执行SETTRANSACTIONISOLATIONLEVEL命令来设置隔离级别。新的隔离级别会在下一个事务开始的时候生效。可以在配置文件中设置整个数据库的隔离级别，也可以只改变当前会话的隔离级别：

```sql
SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;
```

#### 在事务中混合使用存储引擎

MySQL服务器层不管理事务，事务是由下层的存储引擎实现的。所以在同一个事务中，使用多种存储引擎是不可靠的。

如果在事务中混合使用了事务型和非事务型的表（例如InnoDB和MyISAM表），在正常提交的情况下不会有什么问题。但如果该事务需要回滚，非事务型的表上的变更就无法撤销，这会导致数据库处于不一致的状态，这种情况很难修复，事务的最终结果将无法确定。所以，为每张表选择合适的存储引擎非常重要。

#### 隐式和显式锁定

InnoDB 采用的是两阶段锁定协议（two-phase locking protocol）。在事务执行过程中，随时都可以执行锁定，锁只有在执行 COMMIT 或者 ROLLBACK的时候才会释放，并且所有的锁是在同一时刻被释放。前面描述的锁定都是隐式锁定，InnoDB会根据隔离级别在需要的时候自动加锁。

另外，InnoDB也支持通过特定的语句进行显式锁定，这些语句不属于SQL规范:

```sql
SELECT...LOCK IN SHARE MODE
SELECT...FOR UPDATE
```

MySQL也支持 LOCK TABLES 和 UNLOCK TABLES语句，这是在服务器层实现的，和存储引擎无关。它们有自己的用途，但并不能替代事务处理。如果应用需要用到事务，还是应该选择事务型存储引擎。

**除了事务中禁用了AUTOCOMMIT，可以使用 LOCK TABLES之外，其他任何时候都不要显式地执行 LOCK TABLES，不管使用的是什么存储引擎。**

## 1.4 多版本并发控制

MySQL的大多数事务型存储引擎实现的都不是简单的行级锁。基于提升并发性能的考虑，它们一般都同时实现了多版本并发控制（MVCC）。

可以认为MVCC是行级锁的一个变种，但是它在很多情况下避免了加锁操作，因此开销更低。虽然实现机制有所不同，但大都实现了非阻塞的读操作，写操作也只锁定必要的行。

MVCC的实现，是通过保存数据在某个时间点的快照来实现的。也就是说，不管需要执行多长时间，每个事务看到的数据都是一致的。根据事务开始的时间不同，每个事务对同一张表，同一时刻看到的数据可能是不一样的。

不同存储引擎的MVCC实现是不同的，典型的有乐观（optimistic）并发控制和悲观（pessimistic）并发控制。

InnoDB的MVCC，是通过在每行记录后面保存两个隐藏的列来实现的。这两个列，一个保存了行的创建时间，一个保存行的过期时间（或删除时间）。当然存储的并不是实际的时间值，而是系统版本号（system version number）。每开始一个新的事务，系统版本号都会自动递增。事务开始时刻的系统版本号会作为事务的版本号，用来和查询到的每行记录的版本号进行比较。

下面看一下在REPEATABLE READ隔离级别下，MVCC具体是如何操作的。

* SELECT
  * InnoDB会根据以下两个条件检查每行记录：InnoDB只查找版本早于当前事务版本的数据行（也就是，行的系统版本号小于或等于事务的系统版本号），这样可以确保事务读取的行，要么是在事务开始前已经存在的，要么是事务自身插入或者修改过的。行的删除版本要么未定义，要么大于当前事务版本号。这可以确保事务读取到的行，在事务开始之前未被删除。只有符合上述两个条件的记录，才能返回作为查询结果。
* INSERT
  * InnoDB为新插入的每一行保存当前系统版本号作为行版本号。
* DELETE
  * InnoDB为删除的每一行保存当前系统版本号作为行删除标识。
* UPDATE
  * InnoDB为插入一行新记录，保存当前系统版本号作为行版本号，同时保存当前系统版本号到原来的行作为行删除标识。

保存这两个额外系统版本号，使大多数读操作都可以不用加锁。这样设计使得读数据操作很简单，性能很好，并且也能保证只会读取到符合标准的行。不足之处是每行记录都需要额外的存储空间，需要做更多的行检查工作，以及一些额外的维护工作。

**MVCC只在 REPEATABLE READ 和 READ COMMITTED两个隔离级别下工作。** 其他两个隔离级别都和MVCC不兼容，因为 READ UNCOMMITTED 总是读取最新的数据行，而不是符合当前事务版本的数据行。而 SERIALIZABLE 则会对所有读取的行都加锁。

## 1.5 存储引擎

在文件系统中，MySQL将每个数据库（也可以称之为schema）保存为数据目录下的一个子目录。创建表时，MySQL会在数据库子目录下创建一个和表同名的.frm文件保存表的定义。例如创建一个名为MyTable的表，MySQL 会在 MyTable.frm文件中保存该表的定义。因为MySQL使用文件系统的目录和文件来保存数据库和表的定义，大小写敏感性和具体的平台密切相关。在Windows中，大小写是不敏感的；而在类Unix中则是敏感的。不同的存储引擎保存数据和索引的方式是不同的，但表的定义则是在MySQL服务层统一处理的。

```sql
SHOW TABLE STATUS
```

### 1.5.1 InnoDB存储引擎

InnoDB被设计用来处理大量的短期（short-lived）事务，短期事务大部分情况是正常提交的，很少会被回滚。InnoDB的性能和自动崩溃恢复特性，使得它在非事务型存储的需求中也很流行。除非有非常特别的原因需要使用其他的存储引擎，否则应该优先考虑InnoDB引擎。

InnoDB的数据存储在表空间（tablespace）中，表空间是由InnoDB管理的一个黑盒子，由一系列的数据文件组成。在MySQL4.1以后的版本中，InnoDB可以将每个表的数据和索引存放在单独的文件中。InnoDB也可以使用裸设备作为表空间的存储介质，但现代的文件系统使得裸设备不再是必要的选择。InnoDB采用MVCC来支持高并发，并且实现了四个标准的隔离级别。其默认级别是REPEATABLE READ（可重复读），并且通过间隙锁（next-keylocking）策略防止幻读的出现。间隙锁使得InnoDB不仅仅锁定查询涉及的行，还会对索引中的间隙进行锁定，以防止幻影行的插入。

InnoDB表是基于聚簇索引建立的，我们会在后面的章节详细讨论聚簇索引。InnoDB 的索引结构和 MySQL的其他存储引擎有很大的不同，聚簇索引对主键查询有很高的性能。不过它的二级索引（secondary index，非主键索引）中必须包含主键列，所以**如果主键列很大的话，其他的所有索引都会很大**。因此，若表上的索引较多的话，主键应当尽可能的小。InnoDB的存储格式是平台独立的，也就是说可以将数据和索引文件从 Intel 平台复制到 PowerPC或者 SunSPARC平台。

InnoDB内部做了很多优化，包括从磁盘读取数据时采用的可预测性预读，能够自动在内存中创建hash索引以加速读操作的自适应哈希索引（adaptive hash index），以及能够加速插入操作的插入缓冲区（insert buffer）等。本书后面将更详细地讨论这些内容。InnoDB的行为是非常复杂的，不容易理解。如果使用了InnoDB引擎，笔者强烈建议阅读官方手册中的“InnoDB事务模型和锁”一节。如果应用程序基于InnoDB构建，则事先了解一下 InnoDB的 MVCC架构带来的一些微妙和细节之处是非常有必要的。存储引擎要为所有用户甚至包括修改数据的用户维持一致性的视图，是非常复杂的工作。
