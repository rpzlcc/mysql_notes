# 05052017 第一课

## 什么是SQL

* 结构化查询语言是一种特殊目的的编程语言，是一种数据库查询和程序设计语言，用于存取数据以及查询、更新和管理关系数据库系统
* 同时也是数据库脚本文件的扩展名

### SQL的六个部分

1. 数据查询语言（DQL）
    1. 也称数据检索语句，用以从表中获得数据，确定数据怎样在应用程序给出。SELECT是DQL用的最多的动词
    1. 其他保留字有WHERE, ORDER BY, GROUP BY, HAVING等
1. 数据操作语言（DML）
    1. INSERT, UPDATE, DELETE
    1. 用于添加，修改和删除表中的行
1. 事务处理语言（TPL）
    1. 它的语句能确保被DML语句影响的表的所有行能及时得以更新
    1. BEGIN TRANSACTION, COMMIT, ROLLBACK
1. 数据控制语言（DCL）
    1. GRANT, REVOKE
    1. 确定单个用户和用户组对数据库对象的访问
1. 数据定义语言（DDL）
    1. CREATE, DROP
    1. 创建新表或删除表；为表加入索引等
1. 指针控制语言（CCL）
    1. DECLARE CURSOR, FETCH INTO, UPDATE WHERE CURRENT等用于对一个或者多个表单独行的操作

## 主要内容

* SELECT FROM WHERE GROUP BY HAVING ORDER BY 基本语法，执行顺序，注意点
* AND OR LIKE BETWEEN
* IN EXISTS的区别和相同点
* NOT IN的陷阱
* LIMIT
* 理解NULL的一些计算和坑
* 集合的特点
* 几种常用数据类型引起数据类型变化的几种情况；怎么查看是否进行了转换，优点和缺点
* SELECT * 性能上的缺点；网络ORDER BY; JAVA框架中自动生成VO

## 学SQL重点掌握什么

什么是必须记住的，什么是可以Google的

* 基本语法，基本的SQL结构必须记住
    * SELECT, FROM, WHERE, AND, OR, GROUP BY, HAVING, ORDER BY, LIMIT
* 函数可以Google
    * 要分清什么是聚合函数，什么是非聚合函数
    * 聚合函数：COUNT, MIN, MAX, SUM, AVG
    * 非聚合函数：最重要的CASE WHEN
* 使用OR的时候必须养成两边加括号的习惯，否则结果完全不一样
    * or条件必须加括号才能看清楚；同样的条件or的位置不一样结果是完全不同的
    * OR条件如果复杂的情况下，可以适当考虑UNION ALL分离
    * 一对相同的列的or条件等同于in
* AND条件是最基本的
    * AND是交集
    * AND条件越多，结果只能是保持一致或者减少
* LIKE进行模糊条件查询
    * 有索引的列最好进行'aa%'形式，可以使用一些索引
    * 如果非得进行'%aa%'形式的查询，那么这个条件不要作为主要过滤条件
    * 这句话的意思是，此列如果有索引，就不能用索引，即使使用了索引也是对整个索引进行全部查找
    * 日期形式的类型不能进行类似'2008%'的查询，无法使用索引
    * 数字类型有索引，使用'100%'就无法使用索引
* BETWEEN AND和>= <=的特点
* IN和NOT IN
    * 一般的IN相当于OR条件
    * NOT IN不能包含NULL，否则没有结果

必须记住的就是基本的SQL结构

```sql
select s.emp_no, count(distinct e.first_name) as cnt
from salaries s
     inner join employees on e.emp_no = s.emp_no
where s.emp_no in (10001, 10002)
group by s.emp_no
having avg(s.salary) > 1000
order by avg(s.salary)
limit 10;
-- 驱动表： salaries； 有条件
```

SELECT * 的危害

* 因为几乎所有表索引都不可能包括所有列，所以不能进行using index，所以必须回表，导致物理IO增加
* 如果表中有text， blob等字段，表一行超过16k导致行链接
* 第一个原因导致增加对内存的负担，减少内存命中率
* 如果后面有order by，那么对排序有负担
* 增加网络负担
* 有的架构，例如java框架，有自动生成dataset的，会对was服务器内存造成负担


常用的函数

```sql
-- IFNULL, ISNULL
select ifnull(null, 1);
select ifnull(0,1);
select ifnull('', 1), hex('');
select isnull(null), isnull(0), isnull(1/0);

-- time function
select now(), sleep(2), now();
select sysdate(), sleep(2), sysdate();
desc select * from salaries where emp_no = 10001 and from_date > now();
desc select * from salaries where emp_no = 10001 and from_date > sysdate(); -- not as good as using now()
-- sysdate-is-now

-- RPAD, LPAD, RTRIM, LTRIM, TRIM
select rpad('A', 10, '0');
select lpad('A', 10, '0');
select rtrim('A  '), length(rtrim('A  '));
select ltrim('  A'), length(ltrim('   A'));
select trim('  A  '), length(trim('  A  '));

-- concat
select concat('a', 'bc');
select 'A'||'BC'; -- sql_mode = 'PIPES_AS_CONCAT'

-- CASE WHEN THEN END works like if else
select emp_no, first_name,
    case when gender = 'M' then 'Man'
         when gender = 'F' then 'Woman'
         else 'Unknown' end as gender
from employees limit 5;

-- date data type: MySQL doesn't need conversion, as long as the format is a date
desc select emp_no from salaries where emp_no = 20247 and from_date = '1985-03-01';
-- using conversion function
desc select emp_no from salaries where emp_no = 20247 and from_date = str_to_date('1985-03-01', '%Y-%m-%d');

-- NULL
select 1 = 1, null = null, 1 = null, 1 <> null, null is null;
select 1 <=> 1, null <=> null, 1 <=> null, 1 is not null, null is not null;

-- between, in differences
alter table dept_emp2 add index ix_dept_emp (dept_no, emp_no);
desc select * from dept_emp2 force index (ix_dept_emp) where dept_no between 'd003' and 'd005' and emp_no = 10015;
desc select * from dept_emp2 where dept_no in ('d003', 'd004', 'd005') and emp_no = 10005;

-- IN NOT IN
select * from test1 where id not in (1);
select * from test1 where id not in (null);

-- EXISTS, NOT EXISTS
select * from test1 b where not exists (select 1 from test1 a where a.id = b.id and a.id is null); -- 2 rows
select * from test1 b where b.id not in (select a.id from test1 a where a.id is null); -- empty; NOT IN could lose some results; not recommended to use

-- IN, EXISTS
select * from test1 b where exists (select 1 from test1 a where a.id = b.id and a.id is not null);
desc select * from test1 b where exists (select 1 from test1 a where a.id = b.id and a.id is not null);
select * from test1 b where b.id in (select a.id from test1 a where a.id is not null);
desc select * from test1 b where b.id in (select a.id from test1 a where a.id is not null);

-- LIMIT
select * from employees where emp_no between 10001 and 10010 order by first_name limit 0, 5;
desc select * from employees where emp_no between 10001 and 10010 order by first_name limit 0, 5;
```

## 第三课主要内容

### GROUP BY, DISTINCT

GROUP BY即分组，主要作用是把一个集合中根据group by后面的关键字进行分组，内含两个计算

1. 去掉重复值：根据group by后面的内容，使在最终结果中没有重复值
1. 进行排序：对group by后面的关键字进行排序（排序还是order by靠谱）

```sql
select dept_no from t_group group by dept_no;
select distinct(dept_no) from t_group;
desc select dept_no from t_group group by dept_no; -- EXTRA: using temporary; using filesort
desc select distinct(dept_no) from t_group; -- EXTRA: using temporary
```

COUNT, MIN, MAX, SUM, AVG

### ORDER BY

ORDER BY后面可以写列名，也可以写数字（不推荐）

有NULL的时候怎么样？

```sql
select * from t_order order by case when dept_no = 'd008' then '1' else dept_no end asc, emp_no desc;
```

GROUP BY + ORDER BY 利用MySQL特性写出特殊SQL

```sql
-- 找出每个dept_no中的最大emp_no值那一行的所有值
-- 传统方法：1）首先求出每个dept_no组的最大emp_no；2）拿这个值跟原来的表进行join
select t.* from (select dept_no, max(emp_no) emp_no from t_order group by dept_no) a
join t_order t on a.emp_no = t.emp_no and a.dept_no = t.dept_no;

-- 特殊方法：1）先用order by排序成我们想要的结果；2）再用group by特殊用法
select * from t_order order by dept_no asc, emp_no desc;
select a.* from (select * from t_order order by dept_no asc, emp_no desc limit 100) a group by a.dept_no;
```

ORDER BY 两种排序方式

1. single pass
1. two pass

HAVING
SUBQUERY种类
行转列，列转行